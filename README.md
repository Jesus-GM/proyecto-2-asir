# Documentación.
Documentación del proyecto.
- [Información previa de uso.](#informacion-previa-de-uso)
- [Funcionamiento.](#funcionamiento)
- [Estructura del programa.](#estructura)
- [Configuración.](#configuración)
	- [Configuración general.](#configuración-general-del-programa)
	- [Creación de máquinas.](#configuración-de-creación-de-máquinas)
	- [Servicios y aplicaciones.](#configuración-de-servicios-y-aplicaciones)
	- [Comprobaciones.](#comprobaciones)
	- [Dependencias e incompatibilidades.](#dependencias-e-incompatibilidades)
	- [Ejemplos.](program/conf-sample.yaml)

## Información previa de uso.
El programa está testeado y pensado para ser utilizado con las siguientes versiones y paquetes:

### Versiones.
Versiones del software utilizado:
- **S.O:** Debian Buster.
- **Vagrant & Virtual Box.**
- **Apache2:** 2.4
- **NginX:** 1.14
- **PhP:** 7.3
- **MariaDB:** 10.3
- **PhPMyAdmin:** 5.0.4
- **Entorno Virtual & Lenguaje de programación:** Python 3.
- **Ansible:** 2.10
- **Jinja2:** 2.11
- **PyYaml:** 5.3
- **Wordpress:** 5.4
- **Nextcloud:** 20.0.3
- **Prestashop:** 1.7.7

### Paquetes utilizados.
Lista de los posibles paquetes que se instalarán en las MV dependiendo de las distintas configuraciones:
- *unzip*
- *apache2*
- *nginx*
- *mariadb-server*
- *libapache2-mod-php*
- *php-fpm*
- *php-mysql*
- *php-mbstring*
- *php-zip*
- *php-curl*
- *php-gd*
- *php-xml*
- *php-json*
- *php-intl*

## Funcionamiento.
El programa comienza leyendo la configuración del fichero (`conf.yaml`), una vez cargada, se realizan distintas operaciones sobre la misma:
- Se revisa en busca de posibles errores que puedan afectar a la ejecución del programa (`check_conf_functions.py`).
- Se realizan una serie de pasos previos, como por ejemplo, crear la estructura de directorios necesaria para la ejecución (`general_functions.py`).
- Se renderizan las distintas plantillas acorde a las configuración elegida (`render_functions.py`).
Una vez ejecutado el programa correctamente, se procede a levantar las máquinas Vagrant de forma manual, o bien se levantarán de forma automática si así se ha indicado.

## Estructura.
El programa consiste en 5 ficheros principales; fichero de configuración, programa principal, funciones, y 1 directorio donde se almacenan las distintas plantillas.

La estructura de ficheros y directorios sería la siguiente:
- `program/`: Directorio del programa.
	- `conf.yaml`: Fichero de configuración.
	- `main.py`: Fichero con el código principal del programa.
	- `functions/`: Directorio de almacenamiento de las distintas funciones.
		- `general_functions.py`: Funciones de uso general, por ejemplo, generar la estructura de directorios necesaria para los ficheros generados.
		- `check_conf_functions.py`: Funciones de comprobación de la configuración, asegurandose de que sea correcta y así evitar fallos durante la ejecución del programa.
		- `render_functions.py`: Funciones de renderizado de las distintas plantillas.
	- `templates`: Directorio donde se almacenan las distintas plantillas.
		- `vagrant/`: Directorio de plantillas relacionadas con **Vagrant**.
			- `Vagrantfile.j2`: Plantilla de generación de máquinas en **Vagrant**.
		- `ansible/`: Directorio de plantillas relacionadas con **Ansible**.
			- `playbook_web.yaml.j2`: Playbook para la instalación y configuración del servidor web así como la creación de un virtualhost y el CMS indicado.
			- `playbook_db.yaml.j2`: Playbook para la instalación y configuración del servidor de base de datos así como la creación de la base de datos y usuario indicado.
			- `roles/web/`: Directorio de plantillas relacionadas con servidores web.
				- `apache2.conf.j2`: Plantilla del fichero de configuración general de **Apache**.
				- `apache_virtualhost.conf.j2`: Plantilla de configuración del virtualhost para **Apache**.
				- `nginx_virtualhost.conf.j2`:  Plantilla de configuración del virtualhost para **Nginx**.
			- `roles/db/`: Directorio de plantillas relacionadas con servidores de base de datos.
				- `50-server.cnf.j2`: Plantilla de configuración del acceso remoto a la base de datos.
				- `creacion_db_user.sql.j2`: Plantilla de creación de base de datos y usuario.
			- `roles/cms/`: Directorio de plantillas relacionadas con los distintos CMS.
				- `wp-config.php.j2`: Plantilla de configuración de **Wordpress**.
				- `parameters.yml.dist.j2`: Plantilla de configuración de **Prestashop**.

## Configuración.
La configuración se realiza a través del fichero `conf.yaml`, en el que se detallarán los parámetros deseados para el funcionamiento del programa, la creación de las máquinas y el comportamiento de los distintos servicios.

### Configuración general del programa.
- `conf_general`: Inicio de la configuración del programa.
	- `workdir`: [*Str*] Indica la ruta absoluta o relativa para el directorio de trabajo para el programa en el que se crearán los ficheros resultantes de la configuración.
	- `start: { y | n }`: Ejecuta el comando de inicio de Vagrant (`vagrant up`) en el directorio de trabajo del programa (`workdir`).

### Configuración de creación de máquinas.
- `conf_serv_{ web | db }`: [**Obligatorio: Web**] Inicio de la creación de una máquina.
	- `hostname`: [**Obligatorio**, *Str*] Nombre de la máquina.
	- `box`: [**Obligatorio**, *Str*] Box a utilizar por la máquina.
	- `ram`: [*Int*] Cantidad de RAM de la máquina.
	- `ip`: [**Obligatorio: DB, Web (Ver Dependencias)**] Dirección IP a utilizar por la máquina.
	- `public_network: { y | n }`: Crea una red en modo bridge.
	- `adapter`: [*Str*] Interfaz a utilizar por el bridge.
	- `virtualbox_net`: [*Str*] Red privada a utilizar por VirtualBox.

### Configuración de servicios y aplicaciones.
#### Web
- `conf_web`: [**Obligatorio**] Inicio de la configuración del servidor web.
	- `servidor: { apache | nginx }`: [**Obligatorio**] Servicio a utilizar por el servidor web.
	- `aplicacion: { wordpress | nextcloud | prestashop}`: [**Obligatorio**] Aplicación a configurar.
	- `servername`: [**Obligatorio**, *Str*] Nombre de dominio para acceder a la web.
	- `serveralias`: [*Str*, *List*] Lista con los distintos alias para acceder a la web (No incluir `servername`).
	- `vhost_loc`: [*Default: /var/www/html/*, *Str*] Directorio para el contenido de los virtual host (**Ej:** `/usr/share/ | /var/www/ | /srv/www/`).

#### Base de datos.
- `conf_db`: [**Obligatorio**] Inicio de la configuración de base de datos.
	- `modo: { local | dedicado }`: [*Default: local*] Modo de instalación del servicio, en la misma máquina o en una dedicada.
	- `phpmyadmin: { y | n }`: [*Default: n*] Instalación de PhPMyAdmin (`dominio.com/phpmyadmin/`).
	- `nombre`: [**Obligatorio**, *Str*] Nombre de la base de datos.
	- `usuario`: [**Obligatorio**, *Str*] Usuario de la base de datos.
	- `pass`: [**Obligatorio**, *Str*] Contraseña del usuario.

### Comprobaciones.
- Fichero de configuración vacio.
- Categorias vacias.
- Parámetros en blanco.
- Parámetros obligatorios.
- Valores de parámetros correctos.
	- Valores dentro de los rangos indicados.
	- **conf_general.workdir:** Termina por */*.
	- **conf_web.vhost_loc:** Empieza y termina por */*.

### Dependencias e incompatibilidades.
#### Creación de máquinas.
- Al configurar una red en modo puente (`public_network`), **debe** configurarse una interfaz para la misma (`adapter`).

#### Configuración de servicios.
- `conf_db`: Si se configura como *dedicado* (`modo`), **debe** crearse una máquina para ello.
- `conf_db`: Si se configura como *dedicado* (`modo`), **debe** indicarse una IP (`ip`) en el servidor web (`conf_serv_web`) para permitir el acceso a la base de datos remota.
