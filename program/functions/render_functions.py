#---------- IMPORTS ----------#
import os
import jinja2
#---------- FIN IMPORTS ---------#

def render_templates (conf_general, conf_serv_web, conf_web, conf_serv_db, conf_db):
	# Vagrant.
	render_vagrantfile(conf_general, conf_serv_web, conf_serv_db, conf_db)
	# Ansible: web
	render_ansible_roles_web_files(conf_general, conf_web, conf_db)
	# Ansible: db
	render_ansible_roles_db_files(conf_general, conf_serv_web, conf_serv_db, conf_db)
	# Ansible: apps
	render_ansible_roles_apps_files(conf_general, conf_web, conf_serv_db, conf_db)
	# Ansible: Playbooks
	render_ansible_playbooks(conf_general, conf_serv_web, conf_web, conf_db)
	return



def render_vagrantfile (conf_general, conf_serv_web, conf_serv_db, conf_db):
	loader = jinja2.FileSystemLoader("templates/vagrant/")
	env = jinja2.Environment(loader=loader)
	plantilla = env.get_template("Vagrantfile.j2")
	render = plantilla.render(conf_serv_web=conf_serv_web, conf_serv_db=conf_serv_db, conf_db=conf_db)
	if conf_general != 0 and "workdir" in conf_general:
		with open(conf_general["workdir"] + "Vagrantfile", "w") as file:
			file.write(render)
	else:
		with open("Vagrantfile", "w") as file:
			file.write(render)
	return



def render_ansible_roles_web_files(conf_general, conf_web, conf_db):
	loader = jinja2.FileSystemLoader("templates/ansible/roles/web/")
	env = jinja2.Environment(loader=loader)
	plantilla = env.get_template("apache.conf.j2")
	render = plantilla.render(conf_web=conf_web)
	if conf_general != 0 and "workdir" in conf_general:
		with open(conf_general["workdir"] + "roles/web/files/" + "apache.conf", "w") as file:
			file.write(render)
	else:
		with open("roles/web/files/" + "apache.conf", "w") as file:
			file.write(render)
	
	if conf_web["servidor"] == "apache":
		plantilla = env.get_template("apache_virtualhost.conf.j2")
		render = plantilla.render(conf_web=conf_web, conf_db=conf_db)
		if conf_general != 0 and "workdir" in conf_general:
			with open(conf_general["workdir"] + "roles/web/files/" + conf_web["servername"] + ".conf", "w") as file:
				file.write(render)
		else:
			with open("roles/web/files/" + conf_web["servername"] + ".conf", "w") as file:
				file.write(render)
		return
	elif conf_web["servidor"] == "nginx":
		plantilla = env.get_template("nginx_virtualhost.conf.j2")
		render = plantilla.render(conf_web=conf_web, conf_db=conf_db)
		if conf_general != 0 and "workdir" in conf_general:
			with open(conf_general["workdir"] + "roles/web/files/" + conf_web["servername"] + ".conf", "w") as file:
				file.write(render)
		else:
			with open("roles/web/files/" + conf_web["servername"] + ".conf", "w") as file:
				file.write(render)
		return



def render_ansible_roles_db_files(conf_general, conf_serv_web, conf_serv_db, conf_db):
	loader = jinja2.FileSystemLoader("templates/ansible/roles/db/")
	env = jinja2.Environment(loader=loader)
	plantilla = env.get_template("50-server.cnf.j2")
	render = plantilla.render(conf_db=conf_db, conf_serv_db=conf_serv_db)
	if conf_general != 0 and "workdir" in conf_general:
		with open(conf_general["workdir"] + "roles/db/files/" + "50-server.cnf", "w") as file:
			file.write(render)
	else:
		with open("roles/db/files/" + "50-server.cnf", "w") as file:
			file.write(render)
	
	plantilla = env.get_template("creacion_db_user.sql.j2")
	render = plantilla.render(conf_db=conf_db, conf_serv_web=conf_serv_web)
	if conf_general != 0 and "workdir" in conf_general:
		with open(conf_general["workdir"] + "roles/db/files/" + "creacion_db_user.sql", "w") as file:
			file.write(render)
	else:
		with open("roles/db/files/" + "creacion_db_user.sql", "w") as file:
			file.write(render)
	return



def render_ansible_roles_apps_files(conf_general, conf_web, conf_serv_db, conf_db):
	if "phpmyadmin" in conf_db and conf_db["phpmyadmin"] == "y":
		loader = jinja2.FileSystemLoader("templates/ansible/roles/apps/")
		env = jinja2.Environment(loader=loader)
		plantilla = env.get_template("config.inc.php.j2")
		render = plantilla.render(conf_serv_db=conf_serv_db, conf_db=conf_db)
		if conf_general != 0 and "workdir" in conf_general:
			with open(conf_general["workdir"] + "roles/apps/files/" + "config.inc.php", "w") as file:
				file.write(render)
		else:
			with open("roles/apps/files/" + "config.inc.php", "w") as file:
				file.write(render)

	if conf_web["aplicacion"] != "nextcloud":
		loader = jinja2.FileSystemLoader("templates/ansible/roles/apps/")
		env = jinja2.Environment(loader=loader)
		if conf_web["aplicacion"] == "wordpress":
			plantilla = env.get_template("wp-config.php.j2")
			render = plantilla.render(conf_serv_db=conf_serv_db, conf_db=conf_db)
		elif conf_web["aplicacion"] == "prestashop":
			plantilla = env.get_template("parameters.yml.dist.j2")
			render = plantilla.render(conf_serv_db=conf_serv_db, conf_db=conf_db)
		if conf_general != 0 and "workdir" in conf_general:
			if conf_web["aplicacion"] == "wordpress":
				with open(conf_general["workdir"] + "roles/apps/files/" + "wp-config.php", "w") as file:
					file.write(render)
			elif conf_web["aplicacion"] == "prestashop":
				with open(conf_general["workdir"] + "roles/apps/files/" + "parameters.yml.dist", "w") as file:
					file.write(render)
		else:
			if conf_web["aplicacion"] == "wordpress":
				with open("roles/apps/files/" + "wp-config.php", "w") as file:
					file.write(render)
			elif conf_web["aplicacion"] == "prestashop":
				with open("roles/apps/files/" + "parameters.yml.dist", "w") as file:
					file.write(render)



def render_ansible_playbooks(conf_general, conf_serv_web, conf_web, conf_db):
	loader = jinja2.FileSystemLoader("templates/ansible/")
	env = jinja2.Environment(loader=loader)
	plantilla = env.get_template("playbook_web.yaml.j2")
	render = plantilla.render(conf_web=conf_web, conf_db=conf_db)
	if conf_general != 0 and "workdir" in conf_general:
		with open(conf_general["workdir"] + "playbook_web.yaml", "w") as file:
			file.write(render)
	else:
		with open("playbook_web.yaml", "w") as file:
			file.write(render)
	
	plantilla = env.get_template("playbook_db.yaml.j2")
	render = plantilla.render(conf_db=conf_db, conf_serv_web=conf_serv_web)
	if conf_general != 0 and "workdir" in conf_general:
		with open(conf_general["workdir"] + "playbook_db.yaml", "w") as file:
			file.write(render)
	else:
		with open("playbook_db.yaml", "w") as file:
			file.write(render)
	return