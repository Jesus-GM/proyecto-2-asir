#---------- IMPORTS ----------#
import os
#---------- FIN IMPORTS ---------#

def create_ansible_structure(conf_general):
	if conf_general != 0 and "workdir" in conf_general:
		try:
			os.mkdir(conf_general["workdir"] + "roles")
		except FileExistsError:
			pass
		try:
			os.mkdir(conf_general["workdir"] + "roles/web")
		except FileExistsError:
			pass
		try:
			os.mkdir(conf_general["workdir"] + "roles/web/files")
		except FileExistsError:
			pass
		try:
			os.mkdir(conf_general["workdir"] + "roles/db")
		except FileExistsError:
			pass
		try:
			os.mkdir(conf_general["workdir"] + "roles/db/files")
		except FileExistsError:
			pass
		try:
			os.mkdir(conf_general["workdir"] + "roles/apps")
		except FileExistsError:
			pass
		try:
			os.mkdir(conf_general["workdir"] + "roles/apps/files")
		except FileExistsError:
			pass
	else:
		try:
			os.mkdir("roles")
		except FileExistsError:
			pass
		try:
			os.mkdir("roles/web")
		except FileExistsError:
			pass
		try:
			os.mkdir("roles/web/files")
		except FileExistsError:
			pass
		try:
			os.mkdir("roles/db")
		except FileExistsError:
			pass
		try:
			os.mkdir("roles/db/files")
		except FileExistsError:
			pass
		try:
			os.mkdir("roles/apps")
		except FileExistsError:
			pass
		try:
			os.mkdir("roles/apps/files")
		except FileExistsError:
			pass
	return



def start_mvs(conf_general):
	if conf_general != 0:
		if "workdir" in conf_general:
			os.chdir(conf_general["workdir"])
			os.system("vagrant up")
		else:
			os.system("vagrant up")
	return