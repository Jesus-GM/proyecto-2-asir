#-------- CREACIÓN EXCEPCIONES --------#
class ex_error(Exception):
	pass
#-------- VARIABLES --------#

#------------------------------ FUNCIÓN PRINCIPAL ------------------------------#
def check_conf(conf_general, conf_serv_web, conf_web, conf_serv_db, conf_db):
	print ("Comprobando configuración...")
	try:
		# Fichero de configuración vacio.
		check_conf_file_vacio(conf_general, conf_serv_web, conf_web, conf_serv_db, conf_db)
		# Parámetros no añadidos.
		check_conf_general_no_params(conf_general)
		check_conf_serv_web_no_params(conf_serv_web)
		check_conf_serv_db_no_params(conf_serv_db)
		check_conf_web_no_params(conf_web)
		check_conf_db_no_params(conf_db)
		# Valores en blanco.
		check_conf_general_blank_values(conf_general)
		check_conf_serv_web_blank_values(conf_serv_web)
		check_conf_serv_db_blank_values(conf_serv_db)
		check_conf_web_blank_values(conf_web)
		check_conf_db_blank_values(conf_db)
		# Parámetros obligatorios.
		check_conf_serv_web_params_obligatorios(conf_serv_web)
		check_conf_serv_db_params_obligatorios(conf_serv_db)
		check_conf_web_params_obligatorios(conf_web)
		check_conf_db_params_obligatorios(conf_db)
		# Valores correctos
		check_conf_general_valid_values(conf_general)
		check_conf_serv_web_valid_values(conf_serv_web)
		check_conf_serv_db_valid_values(conf_serv_db)
		check_conf_web_valid_values(conf_web)
		check_conf_db_valid_values(conf_db)
		# Dependencias.
		check_conf_serv_web_public_network(conf_serv_web)
		check_conf_serv_db_public_network(conf_serv_db)
		check_conf_serv_db_dedicado(conf_serv_db, conf_db)
		check_conf_db_dedicado_conf_serv_web_ip(conf_serv_web, conf_db)
	except ex_error:
		return
#-------------------------------------------------------------------------------#












#------------------------------ CONF FILE VACIO ------------------------------#
def check_conf_file_vacio(conf_general, conf_serv_web, conf_web, conf_serv_db, conf_db):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_file_vacio(Exception):
		pass
#-------- VARIABLES --------#
	conf_file = [conf_general, conf_serv_web, conf_web, conf_serv_db, conf_db]
	total = 0
#---------------------------#
	try:
		for elem in conf_file:
			if elem == 0:
				total += 1		
		if total == 5:
			raise ex_conf_file_vacio
	except ex_conf_file_vacio:
		print("[ ERROR ] El fichero de configuración está vacio.")
		raise ex_error
	return
#-----------------------------------------------------------------------------#












#------------------------------ PARÁMETROS NO AÑADIDOS (TypeError) ------------------------------#
def check_conf_general_no_params(conf_general):
#-------- VARIABLES --------#
	parametros = ["workdir", "start"]
#---------------------------#
	if conf_general != 0:
		try:
			for elem in parametros:
				if elem in conf_general:
					pass
		except TypeError:
			print("[ ERROR: CONF_GENERAL ] Está vacio, no se ha configurado ningún parámetro.")
			raise ex_error
	else:
		return



def check_conf_serv_web_no_params(conf_serv_web):
#-------- VARIABLES --------#
	parametros = ["hostname", "box", "ram", "ip", "public_network", "adapter", "virtualbox_net"]
#---------------------------#
	if conf_serv_web != 0:
		try:
			for elem in parametros:
				if elem in conf_serv_web:
					pass
		except TypeError:
			print("[ ERROR: CONF_SERV_WEB ] Está vacio, no se ha configurado ningún parámetro.")
			raise ex_error
	else:
		return



def check_conf_serv_db_no_params(conf_serv_db):
#-------- VARIABLES --------#
	parametros = ["hostname", "box", "ram", "ip", "public_network", "adapter", "virtualbox_net"]
#---------------------------#
	if conf_serv_db != 0:
		try:
			for elem in parametros:
				if elem in conf_serv_db:
					pass
		except TypeError:
			print("[ ERROR: CONF_SERV_DB ] Está vacio, no se ha configurado ningún parámetro.")
			raise ex_error
	else:
		return



def check_conf_web_no_params(conf_web):
#-------- VARIABLES --------#
	parametros = ["servidor", "aplicacion", "servername", "serveralias", "vhost_loc"]
#---------------------------#
	if conf_web != 0:
		try:
			for elem in parametros:
				if elem in conf_web:
					pass
		except TypeError:
			print("[ ERROR: CONF_WEB ] Está vacio, no se ha configurado ningún parámetro.")
			raise ex_error
	else:
		return



def check_conf_db_no_params(conf_db):
#-------- VARIABLES --------#
	parametros = ["modo", "phpmyadmin", "nombre", "usuario", "pass"]
#---------------------------#
	if conf_db != 0:
		try:
			for elem in parametros:
				if elem in conf_db:
					pass
		except TypeError:
			print("[ ERROR: CONF_DB ] Está vacio, no se ha configurado ningún parámetro.")
			raise ex_error
	else:
		return
#------------------------------------------------------------------------------------------------#












#------------------------------ PARÁMETROS EN BLANCO ------------------------------#
def check_conf_general_blank_values(conf_general):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_general_blank_workdir(Exception):
		pass
	
	class ex_conf_general_blank_start(Exception):
		pass
#--------------------------------------#
	if conf_general != 0:
		try:
			if "workdir" in conf_general and conf_general["workdir"] == None:
				raise ex_conf_general_blank_workdir
			elif "start" in conf_general and conf_general["start"] == None:
				raise ex_conf_general_blank_start
		except ex_conf_general_blank_workdir:
			print("[ ERROR: CONF_GENERAL ] El parámetro 'workdir' está en blanco.")
			raise ex_error
		except ex_conf_general_blank_start:
			print("[ ERROR: CONF_GENERAL ] El parámetro 'start' está en blanco.")
			raise ex_error
	else:
		return



def check_conf_serv_web_blank_values(conf_serv_web):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_serv_web_blank_hostname(Exception):
		pass

	class ex_conf_serv_web_blank_box(Exception):
		pass

	class ex_conf_serv_web_blank_ram(Exception):
		pass

	class ex_conf_serv_web_blank_ip(Exception):
		pass

	class ex_conf_serv_web_blank_public_network(Exception):
		pass

	class ex_conf_serv_web_blank_adapter(Exception):
		pass
	
	class ex_conf_serv_web_blank_virtualbox_net(Exception):
		pass
#--------------------------------------#
	if conf_serv_web != 0:
		try:
			if "hostname" in conf_serv_web and conf_serv_web["hostname"] == None:
				raise ex_conf_serv_web_blank_hostname
			elif "box" in conf_serv_web and conf_serv_web["box"] == None:
				raise ex_conf_serv_web_blank_box
			elif "ram" in conf_serv_web and conf_serv_web["ram"] == None:
				raise ex_conf_serv_web_blank_ram
			elif "ip" in conf_serv_web and conf_serv_web["ip"] == None:
				raise ex_conf_serv_web_blank_ip
			elif "public_network" in conf_serv_web and conf_serv_web["public_network"] == None:
				raise ex_conf_serv_web_blank_public_network
			elif "adapter" in conf_serv_web and conf_serv_web["adapter"] == None:
				raise ex_conf_serv_web_blank_adapter
			elif "virtualbox_net" in conf_serv_web and conf_serv_web["virtualbox_net"] == None:
				raise ex_conf_serv_web_blank_virtualbox_net
		except ex_conf_serv_web_blank_hostname:
			print("[ ERROR: CONF_SERV_WEB ] El parámetro 'hostname' está en blanco.")
			raise ex_error
		except ex_conf_serv_web_blank_box:
			print("[ ERROR: CONF_SERV_WEB ] El parámetro 'box' está en blanco.")
			raise ex_error
		except ex_conf_serv_web_blank_ram:
			print("[ ERROR: CONF_SERV_WEB ] El parámetro 'ram' está en blanco.")
			raise ex_error
		except ex_conf_serv_web_blank_ip:
			print("[ ERROR: CONF_SERV_WEB ] El parámetro 'ip' está en blanco.")
			raise ex_error
		except ex_conf_serv_web_blank_public_network:
			print("[ ERROR: CONF_SERV_WEB ] El parámetro 'public_network' está en blanco.")
			raise ex_error
		except ex_conf_serv_web_blank_adapter:
			print("[ ERROR: CONF_SERV_WEB ] El parámetro 'adapter' está en blanco.")
			raise ex_error
		except ex_conf_serv_web_blank_virtualbox_net:
			print("[ ERROR: CONF_SERV_WEB ] El parámetro 'virtualbox_net' está en blanco.")
			raise ex_error
	else:
		return



def check_conf_serv_db_blank_values(conf_serv_db):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_serv_db_blank_hostname(Exception):
		pass

	class ex_conf_serv_db_blank_box(Exception):
		pass

	class ex_conf_serv_db_blank_ram(Exception):
		pass

	class ex_conf_serv_db_blank_ip(Exception):
		pass

	class ex_conf_serv_db_blank_public_network(Exception):
		pass

	class ex_conf_serv_db_blank_adapter(Exception):
		pass
	
	class ex_conf_serv_db_blank_virtualbox_net(Exception):
		pass
#--------------------------------------#
	if conf_serv_db != 0:
		try:
			if "hostname" in conf_serv_db and conf_serv_db["hostname"] == None:
				raise ex_conf_serv_db_blank_hostname
			elif "box" in conf_serv_db and conf_serv_db["box"] == None:
				raise ex_conf_serv_db_blank_box
			elif "ram" in conf_serv_db and conf_serv_db["ram"] == None:
				raise ex_conf_serv_db_blank_ram
			elif "ip" in conf_serv_db and conf_serv_db["ip"] == None:
				raise ex_conf_serv_db_blank_ip
			elif "public_network" in conf_serv_db and conf_serv_db["public_network"] == None:
				raise ex_conf_serv_db_blank_public_network
			elif "adapter" in conf_serv_db and conf_serv_db["adapter"] == None:
				raise ex_conf_serv_db_blank_adapter
			elif "virtualbox_net" in conf_serv_db and conf_serv_db["virtualbox_net"] == None:
				raise ex_conf_serv_db_blank_virtualbox_net
		except ex_conf_serv_db_blank_hostname:
			print("[ ERROR: CONF_SERV_DB ] El parámetro 'hostname' está en blanco.")
			raise ex_error
		except ex_conf_serv_db_blank_box:
			print("[ ERROR: CONF_SERV_DB ] El parámetro 'box' está en blanco.")
			raise ex_error
		except ex_conf_serv_db_blank_ram:
			print("[ ERROR: CONF_SERV_DB ] El parámetro 'ram' está en blanco.")
			raise ex_error
		except ex_conf_serv_db_blank_ip:
			print("[ ERROR: CONF_SERV_DB ] El parámetro 'ip' está en blanco.")
			raise ex_error
		except ex_conf_serv_db_blank_public_network:
			print("[ ERROR: CONF_SERV_DB ] El parámetro 'public_network' está en blanco.")
			raise ex_error
		except ex_conf_serv_db_blank_adapter:
			print("[ ERROR: CONF_SERV_DB ] El parámetro 'adapter' está en blanco.")
			raise ex_error
		except ex_conf_serv_db_blank_virtualbox_net:
			print("[ ERROR: CONF_SERV_DB ] El parámetro 'virtualbox_net' está en blanco.")
			raise ex_error
	else:
		return



def check_conf_web_blank_values(conf_web):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_web_blank_servidor(Exception):
		pass

	class ex_conf_web_blank_aplicacion(Exception):
		pass

	class ex_conf_web_blank_servername(Exception):
		pass

	class ex_conf_web_blank_serveralias(Exception):
		pass

	class ex_conf_web_blank_serveralias_element(Exception):
		pass

	class ex_conf_web_blank_vhost_loc(Exception):
		pass
#--------------------------------------#
	if conf_web != 0:
		try:
			if "servidor" in conf_web and conf_web["servidor"] == None:
				raise ex_conf_web_blank_servidor
			elif "aplicacion" in conf_web and conf_web["aplicacion"] == None:
				raise ex_conf_web_blank_aplicacion
			elif "servername" in conf_web and conf_web["servername"] == None:
				raise ex_conf_web_blank_servername
			elif "serveralias" in conf_web and conf_web["serveralias"] == None:
				raise ex_conf_web_blank_serveralias
			elif "vhost_loc" in conf_web and conf_web["vhost_loc"] == None:
				raise ex_conf_web_blank_vhost_loc
			elif "serveralias" in conf_web:
				if type(conf_web["serveralias"]) is list:
					for elem in conf_web["serveralias"]:
						if elem == None:
							raise ex_conf_web_blank_serveralias_element
		except ex_conf_web_blank_servidor:
			print("[ ERROR: CONF_WEB ] El parámetro 'servidor' está en blanco.")
			raise ex_error
		except ex_conf_web_blank_aplicacion:
			print("[ ERROR: CONF_WEB ] El parámetro 'aplicacion' está en blanco.")
			raise ex_error
		except ex_conf_web_blank_servername:
			print("[ ERROR: CONF_WEB ] El parámetro 'servername' está en blanco.")
			raise ex_error
		except ex_conf_web_blank_vhost_loc:
			print("[ ERROR: CONF_WEB ] El parámetro 'vhost_loc' está en blanco.")
			raise ex_error
		except ex_conf_web_blank_serveralias:
			print("[ ERROR: CONF_WEB ] El parámetro 'serveralias' está en blanco.")
			raise ex_error
		except ex_conf_web_blank_serveralias_element:
			print("[ ERROR: CONF_WEB ] Uno de los elementos del parámetro 'start' está en blanco.")
			raise ex_error
	else:
		return



def check_conf_db_blank_values(conf_db):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_db_blank_modo(Exception):
		pass

	class ex_conf_db_blank_phpmyadmin(Exception):
		pass

	class ex_conf_db_blank_nombre(Exception):
		pass

	class ex_conf_db_blank_usuario(Exception):
		pass

	class ex_conf_db_blank_pass(Exception):
		pass
#--------------------------------------#
	if conf_db != 0:
		try:
			if "modo" in conf_db and conf_db["modo"] == None:
				raise ex_conf_db_blank_modo
			elif "phpmyadmin" in conf_db and conf_db["phpmyadmin"] == None:
				raise ex_conf_db_blank_phpmyadmin
			elif "nombre" in conf_db and conf_db["nombre"] == None:
				raise ex_conf_db_blank_nombre
			elif "usuario" in conf_db and conf_db["usuario"] == None:
				raise ex_conf_db_blank_usuario
			elif "pass" in conf_db and conf_db["pass"] == None:
				raise ex_conf_db_blank_pass
		except ex_conf_db_blank_modo:
			print("[ ERROR: CONF_DB ] El parámetro 'modo' está en blanco.")
			raise ex_error
		except ex_conf_db_blank_phpmyadmin:
			print("[ ERROR: CONF_DB ] El parámetro 'phpmyadmin' está en blanco.")
			raise ex_error
		except ex_conf_db_blank_nombre:
			print("[ ERROR: CONF_DB ] El parámetro 'nombre' está en blanco.")
			raise ex_error
		except ex_conf_db_blank_usuario:
			print("[ ERROR: CONF_DB ] El parámetro 'usuario' está en blanco.")
			raise ex_error
		except ex_conf_db_blank_pass:
			print("[ ERROR: CONF_DB ] El parámetro 'pass' está en blanco.")
			raise ex_error
	else:
		return
#----------------------------------------------------------------------------------#












#------------------------------ PARÁMETROS OBLIGATORIOS ------------------------------#
def check_conf_serv_web_params_obligatorios(conf_serv_web):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_serv_web_params_obligatorios_hostname(Exception):
		pass

	class ex_conf_serv_web_params_obligatorios_box(Exception):
		pass
#--------------------------------------#
	if conf_serv_web != 0:
		try:
			if "hostname" not in conf_serv_web:
				raise ex_conf_serv_web_params_obligatorios_hostname
			elif "box" not in conf_serv_web:
				raise ex_conf_serv_web_params_obligatorios_box
		except ex_conf_serv_web_params_obligatorios_hostname:
			print("[ ERROR: CONF_SERV_WEB ] No se ha indicado un nombre (hostname) para el servidor web (conf_serv_web).")
			raise ex_error
		except ex_conf_serv_web_params_obligatorios_box:
			print("[ ERROR: CONF_SERV_WEB ] No se ha indicado una imagen (box) para el servidor web (conf_serv_web).")
			raise ex_error
	else:
		print("[ ERROR: CONF_SERV_WEB ] No se ha configurado la categoría 'conf_serv_web'.")
		raise ex_error



def check_conf_serv_db_params_obligatorios(conf_serv_db):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_serv_db_params_obligatorios_hostname(Exception):
		pass

	class ex_conf_serv_db_params_obligatorios_box(Exception):
		pass

	class ex_conf_serv_db_params_obligatorios_ip(Exception):
		pass
#--------------------------------------#
	if conf_serv_db != 0:
		try:
			if "hostname" not in conf_serv_db:
				raise ex_conf_serv_db_params_obligatorios_hostname
			elif "box" not in conf_serv_db:
				raise ex_conf_serv_db_params_obligatorios_box
			elif "ip" not in conf_serv_db:
				raise ex_conf_serv_db_params_obligatorios_ip
		except ex_conf_serv_db_params_obligatorios_hostname:
			print("[ ERROR: CONF_SERV_DB ] No se ha indicado un nombre (hostname) para el servidor de base de datos (conf_serv_db).")
			raise ex_error
		except ex_conf_serv_db_params_obligatorios_box:
			print("[ ERROR: CONF_SERV_DB ] No se ha indicado una imagen (box) para el servidor de base de datos (conf_serv_db).")
			raise ex_error
		except ex_conf_serv_db_params_obligatorios_ip:
			print("[ ERROR: CONF_SERV_DB ] No se ha indicado una dirección IP para el servidor de base de datos (conf_serv_db).")
			raise ex_error
	else:
		return



def check_conf_web_params_obligatorios(conf_web):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_web_params_obligatorios_aplicacion(Exception):
		pass

	class ex_conf_web_params_obligatorios_servidor(Exception):
		pass
	
	class ex_conf_web_params_obligatorios_servername(Exception):
		pass
#--------------------------------------#
	if conf_web != 0:
		try:
			if "aplicacion" not in conf_web:
				raise ex_conf_web_params_obligatorios_aplicacion
			elif "servidor" not in conf_web:
				raise ex_conf_web_params_obligatorios_servidor
			elif "servername" not in conf_web:
				raise ex_conf_web_params_obligatorios_servername
		except ex_conf_web_params_obligatorios_servidor:
			print("[ ERROR: CONF_WEB ] No se ha configurado el parámetro obligatorio 'servidor'.")
			raise ex_error
		except ex_conf_web_params_obligatorios_servername:
			print("[ ERROR: CONF_WEB ] No se ha configurado el parámetro obligatorio 'servername'.")
			raise ex_error
		except ex_conf_web_params_obligatorios_aplicacion:
			print("[ ERROR: CONF_WEB ] No se ha configurado el parámetro obligatorio 'aplicacion'.")
			raise ex_error
	else:
		print("[ ERROR: CONF_WEB ] No se ha cofigurado la categoría 'conf_web'.")
		raise ex_error



def check_conf_db_params_obligatorios(conf_db):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_db_params_obligatorios_nombre(Exception):
		pass
	
	class ex_conf_db_params_obligatorios_usuario(Exception):
		pass
	
	class ex_conf_db_params_obligatorios_pass(Exception):
		pass
#--------------------------------------#
	if conf_db != 0:
		try:
			if "nombre" not in conf_db:
				raise ex_conf_db_params_obligatorios_nombre
			elif "usuario" not in conf_db:
				raise ex_conf_db_params_obligatorios_usuario
			elif "pass" not in conf_db:
				raise ex_conf_db_params_obligatorios_pass
		except ex_conf_db_params_obligatorios_nombre:
			print("[ ERROR: CONF_DB ] No se ha configurado el parámetro obligatorio 'nombre'.")
			raise ex_error
		except ex_conf_db_params_obligatorios_usuario:
			print("[ ERROR: CONF_DB ] No se ha configurado el parámetro obligatorio 'usuario'.")
			raise ex_error
		except ex_conf_db_params_obligatorios_pass:
			print("[ ERROR: CONF_DB ] No se ha configurado el parámetro obligatorio 'pass'.")
			raise ex_error
	else:
		print("[ ERROR: CONF_DB ] No se ha cofigurado la categoría 'conf_db'.")
		raise ex_error
#-----------------------------------------------------------------------------------#












#------------------------------ PARÁMETROS CON VALORES VALIDOS ------------------------------#
def check_conf_general_valid_values(conf_general):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_general_valid_workdir_type(Exception):
		pass
	
	class ex_conf_general_valid_workdir(Exception):
		pass

	class ex_conf_general_valid_start_type(Exception):
		pass	

	class ex_conf_general_valid_start(Exception):
		pass
#--------------------------------------#
#-------- VARIABLES --------#
	start_values = ["y", "n"]
#---------------------------#
	if conf_general != 0:
		try:
			if "workdir" in conf_general:
				if type(conf_general["workdir"]) is not str:
					raise ex_conf_general_valid_workdir_type
				elif conf_general["workdir"][-1] != "/":
					raise ex_conf_general_valid_workdir
			if "start" in conf_general:
				if type(conf_general["start"]) is not str:
					raise ex_conf_general_valid_start_type
				elif type(conf_general["start"]) is str and conf_general["start"] not in start_values:
					raise ex_conf_general_valid_start
		except ex_conf_general_valid_workdir_type:
			print("[ ERROR: CONF_GENERAL ] El valor configurado en el parámetro 'workdir' no es de tipo String.")
			raise ex_error
		except ex_conf_general_valid_workdir:
			print("[ ERROR: CONF_GENERAL ] El valor configurado en el parámetro 'workdir' no es válido, la ruta debe terminar en '/'.")
			raise ex_error
		except ex_conf_general_valid_start_type:
			print("[ ERROR: CONF_GENERAL ] El valor configurado en el parámetro 'start' no es de tipo String ni List.")
			raise ex_error
		except ex_conf_general_valid_start:
			print("[ ERROR: CONF_GENERAL ] El valor configurado en el parámetro 'start' no es válido.")
			raise ex_error
	else:
		return



def check_conf_serv_web_valid_values(conf_serv_web):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_serv_web_valid_hostname_type(Exception):
		pass

	class ex_conf_serv_web_valid_box_type(Exception):
		pass

	class ex_conf_serv_web_valid_ram_type(Exception):
		pass

	class ex_conf_serv_web_valid_public_network(Exception):
		pass

	class ex_conf_serv_web_valid_adapter_type(Exception):
		pass

	class ex_conf_serv_web_valid_virtualbox_net_type(Exception):
		pass
#--------------------------------------#
#-------- VARIABLES --------#
	public_network_values = ["y", "n"]
#---------------------------#
	if conf_serv_web != 0:
		try:
			if "hostname" in conf_serv_web:
				if type(conf_serv_web["hostname"]) is not str:
					raise ex_conf_serv_web_valid_hostname_type
			if "box" in conf_serv_web:
				if type(conf_serv_web["box"]) is not str:
					raise ex_conf_serv_web_valid_box_type			
			if "ram" in conf_serv_web:
				if type(conf_serv_web["ram"]) is not int:
					raise ex_conf_serv_web_valid_ram_type
			if "public_network" in conf_serv_web:
				if conf_serv_web["public_network"] not in public_network_values:
					raise ex_conf_serv_web_valid_public_network
			if "adapter" in conf_serv_web:
				if type(conf_serv_web["adapter"]) is not str:
					raise ex_conf_serv_web_valid_adapter_type
			if "virtualbox_net" in conf_serv_web:
				if type(conf_serv_web["virtualbox_net"]) is not str:
					raise ex_conf_serv_web_valid_virtualbox_net_type
		except ex_conf_serv_web_valid_hostname_type:
			print("[ ERROR: CONF_SERV_WEB ] El valor configurado en el parámetro 'hostname' no es de tipo String.")
			raise ex_error
		except ex_conf_serv_web_valid_box_type:
			print("[ ERROR: CONF_SERV_WEB ] El valor configurado en el parámetro 'box' no es de tipo String.")
			raise ex_error
		except ex_conf_serv_web_valid_ram_type:
			print("[ ERROR: CONF_SERV_WEB ] El valor configurado en el parámetro 'ram' no es de tipo Int.")
			raise ex_error
		except ex_conf_serv_web_valid_public_network:
			print("[ ERROR: CONF_SERV_WEB ] El valor configurado en el parámetro 'public_network' no es válido.")
			raise ex_error
		except ex_conf_serv_web_valid_adapter_type:
			print("[ ERROR: CONF_SERV_WEB ] El valor configurado en el parámetro 'adapter' no es de tipo String.")
			raise ex_error
		except ex_conf_serv_web_valid_virtualbox_net_type:
			print("[ ERROR: CONF_SERV_WEB ] El valor configurado en el parámetro 'virtualbox_net' no es de tipo String.")
			raise ex_error
	else:
		return



def check_conf_serv_db_valid_values(conf_serv_db):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_serv_db_valid_hostname_type(Exception):
		pass

	class ex_conf_serv_db_valid_box_type(Exception):
		pass

	class ex_conf_serv_db_valid_ram_type(Exception):
		pass

	class ex_conf_serv_db_valid_public_network(Exception):
		pass

	class ex_conf_serv_db_valid_adapter_type(Exception):
		pass

	class ex_conf_serv_db_valid_virtualbox_net_type(Exception):
		pass
#--------------------------------------#
#-------- VARIABLES --------#
	public_network_values = ["y", "n"]
#---------------------------#
	if conf_serv_db != 0:
		try:
			if "hostname" in conf_serv_db:
				if type(conf_serv_db["hostname"]) is not str:
					raise ex_conf_serv_db_valid_hostname_type
			if "box" in conf_serv_db:
				if type(conf_serv_db["box"]) is not str:
					raise ex_conf_serv_db_valid_box_type
			if "ram" in conf_serv_db:
				if type(conf_serv_db["ram"]) is not int:
					raise ex_conf_serv_db_valid_ram_type
			if "public_network" in conf_serv_db:
				if conf_serv_db["public_network"] not in public_network_values:
					raise ex_conf_serv_db_valid_public_network
			if "adapter" in conf_serv_db:
				if type(conf_serv_db["adapter"]) is not str:
					raise ex_conf_serv_db_valid_adapter_type
			if "virtualbox_net" in conf_serv_db:
				if type(conf_serv_db["virtualbox_net"]) is not str:
					raise ex_conf_serv_db_valid_virtualbox_net_type
		except ex_conf_serv_db_valid_hostname_type:
			print("[ ERROR: CONF_SERV_DB ] El valor configurado en el parámetro 'hostname' no es de tipo String.")
			raise ex_error
		except ex_conf_serv_db_valid_box_type:
			print("[ ERROR: CONF_SERV_DB ] El valor configurado en el parámetro 'box' no es de tipo String.")
			raise ex_error
		except ex_conf_serv_db_valid_ram_type:
			print("[ ERROR: CONF_SERV_DB ] El valor configurado en el parámetro 'ram' no es de tipo Int.")
			raise ex_error
		except ex_conf_serv_db_valid_public_network:
			print("[ ERROR: CONF_SERV_DB ] El valor configurado en el parámetro 'public_network' no es válido.")
			raise ex_error
		except ex_conf_serv_db_valid_adapter_type:
			print("[ ERROR: CONF_SERV_DB ] El valor configurado en el parámetro 'adapter' no es de tipo String.")
			raise ex_error
		except ex_conf_serv_db_valid_virtualbox_net_type:
			print("[ ERROR: CONF_SERV_DB ] El valor configurado en el parámetro 'virtualbox_net' no es de tipo String.")
			raise ex_error
	else:
		return



def check_conf_web_valid_values(conf_web):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_web_valid_servidor(Exception):
		pass
	
	class ex_conf_web_valid_aplicacion(Exception):
		pass

	class ex_conf_web_valid_servername_type(Exception):
		pass

	class ex_conf_web_valid_serveralias_type(Exception):
		pass

	class ex_conf_web_valid_serveralias_element_type(Exception):
		pass


	class ex_conf_web_valid_vhost_loc_type(Exception):
		pass
	
	class ex_conf_web_valid_vhost_loc(Exception):
		pass
#--------------------------------------#
#-------- VARIABLES --------#
	servidor_values = ["apache", "nginx"]
	aplicacion_values = ["wordpress", "nextcloud", "prestashop"]
#---------------------------#
	if conf_web != 0:
		try:
			if "servidor" in conf_web:
				if conf_web["servidor"] not in servidor_values:
					raise ex_conf_web_valid_servidor
			if "aplicacion" in conf_web:
				if conf_web["aplicacion"] not in aplicacion_values:
					raise ex_conf_web_valid_aplicacion
			if "servername" in conf_web:
				if type(conf_web["servername"]) is not str:
					raise ex_conf_web_valid_servername_type
			if "serveralias" in conf_web:
				if type(conf_web["serveralias"]) is not str and type(conf_web["serveralias"]) is not list:
					raise ex_conf_web_valid_serveralias_type
				elif type(conf_web["serveralias"]) is list:
					for elem in conf_web["serveralias"]:
						if type(elem) is not str:
							raise ex_conf_web_valid_serveralias_element_type
			if "vhost_loc" in conf_web:
				if type(conf_web["vhost_loc"]) is not str:
					raise ex_conf_web_valid_vhost_loc_type
				elif conf_web["vhost_loc"][0] != "/" or conf_web["vhost_loc"][-1] != "/":
					raise ex_conf_web_valid_vhost_loc
		except ex_conf_web_valid_servidor:
			print("[ ERROR: CONF_WEB ] El valor configurado en el parámetro 'servidor' no es válido.")
			raise ex_error
		except ex_conf_web_valid_aplicacion:
			print("[ ERROR: CONF_WEB ] El valor configurado en el parámetro 'aplicacion' no es válido")
			raise ex_error
		except ex_conf_web_valid_servername_type:
			print("[ ERROR: CONF_WEB ] El valor configurado en el parámetro 'servername' no es de tipo String.")
			raise ex_error
		except ex_conf_web_valid_serveralias_type:
			print("[ ERROR: CONF_WEB ] El valor configurado en el parámetro 'serveralias' no es de tipo String o List.")
			raise ex_error
		except ex_conf_web_valid_serveralias_element_type:
			print("[ ERROR: CONF_WEB ] Uno de los valores configurados en el parámetro 'serveralais' no es de tipo String.")
			raise ex_error
		except ex_conf_web_valid_vhost_loc_type:
			print("[ ERROR: CONF_WEB ] El valor configurado en el parámetro 'vhost_loc' no es de tipo String.")
			raise ex_error
		except ex_conf_web_valid_vhost_loc:
			print("[ ERROR: CONF_WEB ] El valor configurado en el parámetro 'vhost_loc' no es válido, la ruta debe empezar y terminar por '/'.")
			raise ex_error
	else:
		return



def check_conf_db_valid_values(conf_db):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_db_valid_modo(Exception):
		pass

	class ex_conf_db_valid_phpmyadmin(Exception):
		pass

	class ex_conf_db_valid_nombre_type(Exception):
		pass

	class ex_conf_db_valid_usuario_type(Exception):
		pass

	class ex_conf_db_valid_pass_type(Exception):
		pass
#--------------------------------------#
#-------- VARIABLES --------#
	modo_values = ["local", "dedicado"]
	phpmyadmin_values = ["y", "n"]
#---------------------------#
	if conf_db != 0:
		try:
			if "modo" in conf_db:
				if conf_db["modo"] not in modo_values:
					raise ex_conf_db_valid_modo
			if "phpmyadmin" in conf_db:
				if conf_db["phpmyadmin"] not in phpmyadmin_values:
					raise ex_conf_db_valid_phpmyadmin
			if "nombre" in conf_db:
				if type(conf_db["nombre"]) is not str:
					raise ex_conf_db_valid_nombre_type
			if "usuario" in conf_db:
				if type(conf_db["usuario"]) is not str:
					raise ex_conf_db_valid_usuario_type
			if "pass" in conf_db:
				if type(conf_db["pass"]) is not str:
					raise ex_conf_db_valid_pass_type
		except ex_conf_db_valid_modo:
			print("[ ERROR: CONF_DB ] El valor configurado en el parámetro 'modo' no es válido.")
			raise ex_error
		except ex_conf_db_valid_phpmyadmin:
			print("[ ERROR: CONF_DB ] El valor configurado en el parámetro 'phpmyadmin' no es válido.")
			raise ex_error
		except ex_conf_db_valid_nombre_type:
			print("[ ERROR: CONF_DB ] El valor configurado en el parámetro 'nombre' no es de tipo String.")
			raise ex_error
		except ex_conf_db_valid_usuario_type:
			print("[ ERROR: CONF_DB ] El valor configurado en el parámetro 'usuario' no es de tipo String.")
			raise ex_error
		except ex_conf_db_valid_pass_type:
			print("[ ERROR: CONF_DB ] El valor configurado en el parámetro 'pass' no es de tipo String.")
			raise ex_error

	else:
		return
#--------------------------------------------------------------------------------------------#












#------------------------------ DEPENDENCIAS ------------------------------#
# Descripción: Comprueba que la configuración relacionada con public_network es correcta en conf_serv_web.
def check_conf_serv_web_public_network(conf_serv_web):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_serv_web_public_network_adapter(Exception):
		pass

	class ex_conf_serv_web_adapter_public_network(Exception):
		pass
#--------------------------------------#
	try:
		if "public_network" in conf_serv_web:
			if conf_serv_web["public_network"] == "y" and "adapter" not in conf_serv_web:
					raise ex_conf_serv_web_public_network_adapter
			if conf_serv_web["public_network"] == "n" and "adapter" in conf_serv_web:
					raise ex_conf_serv_web_adapter_public_network
		elif "public_network" not in conf_serv_web and "adapter" in conf_serv_web:
				raise ex_conf_serv_web_adapter_public_network
	except ex_conf_serv_web_public_network_adapter:
		print("[ ERROR: CONF_SERV_WEB ] Se ha configurado una red en modo puente (public_network) pero no una interfaz para la misma (adapter).")
		raise ex_error
	except ex_conf_serv_web_adapter_public_network:
		print("[ ERROR: CONF_SERV_WEB ] Se ha configurado una interfaz (adapter) pero no una red para la misma (public_network).")
		raise ex_error
		
	return



# Descripción: Comprueba que la configuración relacionada con public_network es correcta en conf_serv_db.
def check_conf_serv_db_public_network (conf_serv_db):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_serv_db_public_network_adapter(Exception):
		pass

	class ex_conf_serv_db_adapter_public_network(Exception):
		pass
#--------------------------------------#
	if conf_serv_db != 0:
		try:
			if "public_network" in conf_serv_db:
				if conf_serv_db["public_network"] == "y" and "adapter" not in conf_serv_db:
						raise ex_conf_serv_db_public_network_adapter
				if conf_serv_db["public_network"] == "n" and "adapter" in conf_serv_db:
						raise ex_conf_serv_db_adapter_public_network
			elif "public_network" not in conf_serv_db and "adapter" in conf_serv_db:
					raise ex_conf_serv_db_adapter_public_network
		except ex_conf_serv_db_public_network_adapter:
			print("[ ERROR: CONF_SERV_DB ] Se ha configurado una red en modo puente (public_network) pero no una interfaz para la misma (adapter).")
			raise ex_error
		except ex_conf_serv_db_adapter_public_network:
			print("[ ERROR: CONF_SERV_DB ] Se ha configurado una interfaz (adapter) pero no una red para la misma (public_network).")
			raise ex_error
	else:
		return



# Descripción: Comprueba la configuración de un servidor de base de datos dedicado cuando proceda.
def check_conf_serv_db_dedicado(conf_serv_db, conf_db):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_serv_db_dedicado(Exception):
		pass
#--------------------------------------#
	try:
		if "modo" in conf_db:
			if conf_db["modo"] == "dedicado" and conf_serv_db == 0:
				raise ex_conf_serv_db_dedicado
	except ex_conf_serv_db_dedicado:
		print("[ ERROR: SERV_DB ] Se ha configurado como dedicado (modo) pero no se ha creado una máquina para esta configuración (conf_serv_db).")
		raise ex_error
	
	return
#--------------------------------------------------------------------------#


# Descripción: Comprueba que se ha configurado una dirección IP para el acceso remoto a la base de datos.
def check_conf_db_dedicado_conf_serv_web_ip(conf_serv_web, conf_db):
#-------- CREACIÓN EXCEPCIONES --------#
	class ex_conf_db_dedicado_conf_serv_web_ip(Exception):
		pass
#--------------------------------------#
	if "modo" in conf_db and conf_db["modo"] == "dedicado":
		try:
			if "ip" not in conf_serv_web:
				raise ex_conf_db_dedicado_conf_serv_web_ip
		except ex_conf_db_dedicado_conf_serv_web_ip:
			print("[ ERROR: SERV_DB ] Se ha configurado como dedicado (modo) pero no se ha configurado una IP en el servidor web (conf_serv_web) para la configuración de la conexión remota")
			raise ex_error
	return