#---------- IMPORTS ----------#
import yaml
from functions.check_conf_functions import check_conf
from functions.general_functions import *
from functions.render_functions import render_templates
#-----------------------------#

#-------------------- VARIABLES --------------------#
#------------ Yaml Loader ------------#
conf_file = open("conf.yaml")
conf = yaml.load(conf_file, Loader=yaml.FullLoader)
#-------------------------------------#

#------------ General ------------#
try:
	conf_general = conf["conf_general"]
except KeyError:
	conf_general = 0
except TypeError:
	conf_general = 0
#---------------------------------#

#------------ Web ------------#
try:
	conf_serv_web = conf["conf_serv_web"]
except KeyError:
	conf_serv_web = 0
except TypeError:
	conf_serv_web = 0
try:
	conf_web = conf["conf_web"]
except KeyError:
	conf_web = 0
except TypeError:
	conf_web = 0
#-----------------------------#

#------------ DB ------------#
try:
	conf_serv_db = conf["conf_serv_db"]
except KeyError:
	conf_serv_db = 0
except TypeError:
	conf_serv_db = 0
try:
	conf_db = conf["conf_db"]
except KeyError:
	conf_db = 0
except TypeError:
	conf_db = 0
#----------------------------#
#---------------------------------------------------#

#-------------------- COMPROBAR CONFIGURACIÓN --------------------#
check_conf(conf_general, conf_serv_web, conf_web, conf_serv_db, conf_db)
#-----------------------------------------------------------------#

#-------------------- CREACIÓN DE LA ESTRUCTURA DE DIRECTORIOS --------------------#
create_ansible_structure(conf_general)
#----------------------------------------------------------------------------------#

#-------------------- RENDERIZAR PLANTILLAS --------------------#
render_templates(conf_general, conf_serv_web, conf_web, conf_serv_db, conf_db)
#------------------------------------------------------------#

#-------------------- INICIAR MVs --------------------#
if "start" in conf_general and conf_general["start"] == "y":
	start_mvs(conf_general)
#-----------------------------------------------------#